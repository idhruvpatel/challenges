# SRE tech challenge

App can be deployed as a web service in a kubernetes cluster and when invoked (via a simple http GET) returns the same information as if  it was running on the console.

## Notes

- Source code for the application can be found here. [Source Code](https://gitlab.com/idhruvpatel/challenges/-/tree/main/Challenge-Python-K8s)
- I have made few changes in the code so code can use the native functionalities of the python. Changes can be found [here.](https://gitlab.com/idhruvpatel/challenges/-/blob/main/Challenge-Python-K8s/app.py)
- I have added the helm plain kubernetes manifests and a simple helm chart so that by the both ways we can deploy the app.

## Original Code

```
import time
# ts stores the time in seconds
ts = time.time()
# print the current timestamp
print(ts)
```

## Design Decisions

- I have used flask as it is natively compatible with python to server the HTTP GET requests. For acheiving that I have make some changes in the code and I have updated the same above in the notes section.
- I have created the HorizontalPodAutoscaler, in that I have considered only CPU and Memory resources of the containers. I have only experience with them rest I have not worked with before.
- I have created an ingress to serve the stuff via ```example.ai``` domain.  For getting the view of that we need to map ingress IP to domain name in the ```hosts``` file of the respective system where you're testing.

## TL;DR

- **Cloning the repo:**

```
git clone https://gitlab.com/idhruvpatel/challenges/-/tree/main/Challenge-Python-K8s
cd Challenge-Python-K8s
```

- **Method 1 via ```helm```:**

```bash
helm install timestamp-server timestamp-server-helm --namespace=<whatever-namespace-you-want-to-deploy-in>
# For deleting that deployment.
helm uninstall timestamp-server --namespace=<whatever-namespace-you-want-to-deploy-in>
```

- **Method 2 via ```kubectl```:**

```bash
kubectl apply -f ref-k8s.deployment.yaml --namespace=<whatever-namespace-you-want-to-deploy-in>
# For deleting that deployment.
kubectl delete -f ref-k8s.deployment.yaml --namespace=<whatever-namespace-you-want-to-deploy-in>
```
