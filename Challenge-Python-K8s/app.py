from flask import Flask, request, render_template
import time

app = Flask(__name__)

@app.route("/", methods=['GET'])
def getTime():
  # ts stores the time in seconds
  ts = time.time()
  # print the current timestamp
  print(ts)
  return str(ts)

app.run(host='0.0.0.0', port=8000)